
import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
class CellWrapper extends Component {

  componentDidMount() {
    this.props.updateTag && this.props.updateTag(this.refs.view, this.props.sectionId);
  }

  _onLayout(e) {
    const y = e.nativeEvent.layout.y;
    this.props.updateTag && this.props.updateTag(y, this.props.sectionId);
  }

  render() {
    const Cell = this.props.component;
    return (
      <View ref='view' onLayout={this._onLayout.bind(this)}>
        <Cell {...this.props} />
      </View>
    );
  }
}

CellWrapper.propTypes = {

  /**
   * The id of the section
   */
  sectionId: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),

  /**
   * A component to render for each cell
   */
  component: PropTypes.func.isRequired,

  /**
   * A function used to propagate the root nodes handle back to the parent
   */
  updateTag: PropTypes.func

};


module.exports = CellWrapper;
